<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ __('Login') }}</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="moa/images/favicon.ico" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="moa/css/bootstrap.min.css">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="moa/css/typography.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="moa/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="moa/css/responsive.css">
  </head>
  <style>
  @font-face{
    font-family: MontserratBold;
    src: url('/font/Montserrat/Montserrat-Bold.woff');
  }
  @font-face{
    font-family: MontserratRegular;
    src: url('/font/Montserrat/Montserrat-Regular.woff');
  }
  @font-face{
    font-family: MontserratSemiBold;
    src: url('/font/Montserrat/Montserrat-SemiBold.woff');
  }
  .container, body{ width: 100%; max-width: 100% !important; background-color: #FFF !important; overflow: hidden; }
  .sign-in-detail { padding: 0px !important; }
  .sign-in-detail { background: #FFF !important; }

  .dot-large{
    height: 1400px;
    width: 1500px;
    background-color: #C8C1E1;
    border-radius: 50%;
    position: absolute;
    top: -500px;
    right: -700px;
    z-index: 0;
  }
  .dot-shadow{
    height: 1400px;
    width: 1500px;
    background-color: #E8E5F3;
    border-radius: 50%;
    position: absolute;
    top: -450px;
    right: -710px;
    z-index: -1;
  }
  .logo{
    width: 200px;
    margin-top: 1%;
    margin-left: 1%;
  }

  .slider{
    position: absolute;
    width: 900px;
    top: 150px;
    left: 70px;
  }

  .bounce {
    animation: bounce 2s infinite;
  }

  @keyframes bounce {
    0%, 20%, 50%, 80%, 100% {
      transform: translateY(0);
    }
    40% {
      transform: translateY(-7px);
    }
    60% {
      transform: translateY(-3px);
    }
  }

  .wrapper-login{
    position: absolute;
    /* background-color: red; */
    width: 500px;
    height: 500px;
    right: 50px;
    top: 100px;
  }

  .content-login{
    /* background-color: blue; */
    margin-top: 100px;
  }
  </style>
  <body>
    <div class="container">
      <div class="logo">
        <img src="{{ asset('/img/logo/TenantPortal.png') }}" alt="logo" srcset="" style="width: inherit">
      </div>
      <div class="dot-large"></div>
      <div class="dot-shadow"></div>

      <div class="slider bounce">
        <img class="bounce" src="{{ asset('/img/tenant-portal1.png') }}" alt="slider" srcset="" style="width: inherit">
      </div>

      <div class="wrapper-login">
        <div class="title-login">
          <h1 style="font-family: MontserratRegular; color: #333333">Hello !</h1>
          <h3 style="font-family: MontserratSemiBold; color: #553e93; margin-top: -10px;">Welcome Back</h3>
        </div>
        <div class="content-login">
          <center><h3><span style="font-family: MontserratBold; color: #553e93;">Login</span><span style="font-family: MontserratSemiBold; color: #333"> Your Account</span></h3><center>
          <form method="POST" action="{{ url('post-login') }}" class="mt-4">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="input-group mb-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></div>
                </div>
                <input type="text" name="username" id="username" class="form-control mb-0 @if($errors->has('username')) is-invalid @endif" placeholder="Enter Email" value="{{ old('username') }}" required autofocus style="border: 1px solid #333 !important">
                @if($errors->has('username'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('username') }}</strong>
                </span>
              @endif
              </div>
            </div>
            <div class="form-group">
              <input type="password" name="password" id="password" class="form-control mb-0 @if($errors->has('password')) is-invalid @endif" placeholder="Enter Password" value="{{ old('password') }}" required autofocus style="border: 1px solid #333 !important">
              @if($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
            <div class="d-inline-block w-100">
              <button type="submit" class="btn btn-primary float-right">Sign in</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="moa/js/jquery.min.js"></script>
  <script src="moa/js/popper.min.js"></script>
  <script src="moa/js/bootstrap.min.js"></script>
  <!-- Appear JavaScript -->
  <script src="moa/js/jquery.appear.js"></script>
  <!-- Countdown JavaScript -->
  <script src="moa/js/countdown.min.js"></script>
  <!-- Counterup JavaScript -->
  <script src="moa/js/waypoints.min.js"></script>
  <script src="moa/js/jquery.counterup.min.js"></script>
  <!-- Wow JavaScript -->
  <script src="moa/js/wow.min.js"></script>
  <!-- Apexcharts JavaScript -->
  <script src="moa/js/apexcharts.js"></script>
  <!-- Slick JavaScript -->
  <script src="moa/js/slick.min.js"></script>
  <!-- Select2 JavaScript -->
  <script src="moa/js/select2.min.js"></script>
  <!-- Owl Carousel JavaScript -->
  <script src="moa/js/owl.carousel.min.js"></script>
  <!-- Magnific Popup JavaScript -->
  <script src="moa/js/jquery.magnific-popup.min.js"></script>
  <!-- Smooth Scrollbar JavaScript -->
  <script src="moa/js/smooth-scrollbar.js"></script>
  <!-- Chart Custom JavaScript -->
  <script src="moa/js/chart-custom.js"></script>
  <!-- Custom JavaScript -->
  <script src="moa/js/custom.js"></script>
</html>
