<div class="modal fade" id="modal-schedules" data-backdrop="static" data-keyboard="false" tabindex="-1" 
    aria-labelledby="staticBackdropLabel" aria-hidden="true"
>
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table width="100%" id="table-schedules" class="table table-striped table-bordered table-hover" 
                    style="color: #353535;"
                >
                    <thead>
                    <tr>
                        <th scope="col">Barcode</th>
                        <th scope="col">Asset Name</th>
                        <th scope="col">Schedule Date</th>
                        <th scope="col">On Transaction</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>