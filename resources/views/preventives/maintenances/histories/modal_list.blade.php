<!-- Modal -->
<div class="modal fade" id="modal-histories" data-backdrop="static" data-keyboard="false" 
    tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"
>
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Histories</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table-histories" class="table table-striped table-hover display mb-2" 
                        style="color: #353535;width:100%;"
                    >
                        <thead>
                        <tr>
                            <th scope="col">Ticket</th>
                            <th scope="col">Status</th>
                            <th scope="col">Total Value</th>
                            <th scope="col">Location</th>
                            <th scope="col">Barcode</th>
                            <th scope="col">Asset Name</th>
                            <th scope="col">Schedule Date</th>
                            <th scope="col">Assign To</th>
                            <th scope="col">Assign Date</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>