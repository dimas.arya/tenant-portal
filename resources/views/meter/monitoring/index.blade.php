@extends('layouts.crm_main')

@section('content')
@if(empty(Auth::user()))
   <script>
      window.location = "{{ route('logout') }}";
   </script>
@else

<div class="col-sm-12 col-md-12 col-xs-12">
  <div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
      <div class="iq-header-title">
      <h4 class="card-title"><b>Auto Meter Capture Monitoring </b></h4>
      </div>
    </div>
    <hr>
    <div class="iq-card-body center mt-5">
      <div class="row col-md-12 col-sm-12 col-xs-12 mb-3" id="container-content">
      </div>
    </div>
  </div>
</div>

@endif
@endsection

@push('scripts')
<script>
  function getDataAjax(param) {
    $.ajax({
      type: "GET",
      url: "/meter/monitoring/getData",
      data: {
        entity_project:param,
      },
      dataType: "html",
      success: function (response) {
        $('#container-content').html(response);
      }
    });
  }
  getDataAjax($('#filter_entity :selected').val());
  
  $(document).ready(function(){
    setInterval(() => {
      getDataAjax($('#filter_entity :selected').val());
    }, 5000);
  });
</script>
@endpush