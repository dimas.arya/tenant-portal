@extends('layouts.crm_main')

@section('content')
<style>
  .select2.select2-container .select2-selection{
      height: 45px;
      line-height: 45px;
      padding: .375rem .35rem;
      font-size: 14px;
      color: #d7dbda;
      border: 1px solid #d7dbda;
      border-radius: 10px;
  }
  .select2-selection__arrow{
      margin-top: 8px;
  }
</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
  <div class="iq-card">
    <div class="iq-card-header d-flex justify-content-between">
      <div class="iq-header-title mt-3">
        <h4 class="card-title"><i>title</i></h4>
      </div>
    </div>
    <hr>
    <div class="table-responsive center mt-5">
      <div class="col-xs-auto col-sm-auto col-md-auto col-lg-auto mb-3">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 mb-5 pull-right">
          <div class="float-left ml-2" style="width: 30%">
            <select class="select2 form-control" name="entity_project" id="entity_project">
              <option value="">-- Choose --</option>
            </select>
          </div>
          <div class="float-left ml-2" style="width: 30%">
            <select class="select2 form-control" name="type" id="type">
              <option value="">-- Choose --</option>
              <option value="PR">Purchase Request</option>
              <option value="PO">Purchase Order</option>
              <option value="Quotation">Quotation</option>
              <option value="GRN">GRN</option>
              <option value="Invoice">Invoice Receipt</option>
              <option value="Payment">Payment</option>
            </select>
          </div>
          <div class="float-left ml-2" style="width: 35%">
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="period" id="period" placeholder="Period">
              <div class="input-group-prepend">
                <span class="input-group-text" style="border-radius: 0px 5px 5px 0px"><i class="fa fa-calendar" aria-hidden="true"></i></span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="clearfix"></div>
      <hr>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-5">
        <div class="table-responsive center">
          <table class="table display table-striped" id="table_list">
            <thead>
              <tr>
                <th class="text-center" rowspan="2">Entity</th>
                <th class="text-center" rowspan="2">Department</th>
                <th class="text-center" colspan="6">Purchase Request</th>
                <th class="text-center" colspan="2">Quotation</th>
                <th class="text-center" colspan="6">Purchase Order</th>
                <th class="text-center" colspan="5">GRN</th>
                <th class="text-center" colspan="4">Invoice Receipt</th>
                <th class="text-center" colspan="4">Payment</th>
              </tr>
              <tr>
                {{-- pr --}}
                <th>Request No</th>
                <th>Request Date</th>
                <th>Total Item</th>
                <th>Total Qty</th>
                <th>Created At</th>
                <th>Created By</th>
                {{-- end pr --}}
                {{-- quotation --}}
                <th>Effective Date</th>
                <th>Vendor Comparison</th>
                {{-- end quotation --}}
                {{-- po --}}
                <th>Supplier Name</th>
                <th>Order No</th>
                <th>Total Item</th>
                <th>Total Qty</th>
                <th>Order Date</th>
                <th>Created By</th>
                {{-- end po --}}
                {{-- grn --}}
                <th>GRN No</th>
                <th>Total Item</th>
                <th>Total Qty</th>
                <th>Delivery Date</th>
                <th>Created By</th>
                {{-- end grn --}}
                {{-- invoice --}}
                <th>Transaction Date</th>
                <th>Document No</th>
                <th>Total Invoice Value</th>
                <th>Created By</th>
                {{-- end invoice --}}
                {{-- payment --}}
                <th>Document No</th>
                <th>Document Date</th>
                <th>Total Invoice Value</th>
                <th>Created By</th>
                {{-- end payment --}}
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
@include('fiveWay.scripts.scriptIndex')
@endpush