<script>
  $(document).ready(function(){
    $('#table_list').DataTable();
    
    function loadTable(){
      var table  = $('#table_list');

      table.DataTable().clear().destroy();

      table.DataTable({
        // scrollX: true,
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
          method: 'POST',
          url: '/five-way/listData',
          data: {
            _token: "{{ csrf_token() }}",
            entity_project: $('#entity_project :selected').val(),
            type: $('#type :selected').val(),
            period: $('#period').val(),
          }
        },
        columns:[
          {data:'entity_name'},
          {data:'dept'},
          {data:'request_no'},
          {data:'request_date'},
          {data:'total_item_cd_pr'},
          {data:'total_qty_pr'},
          {data:'audit_date_pr'},
          {data:'pr_name'},

          {data:'quotation_date'},
          {data:'vendor_comparisson'},

          {data:'name'},
          {data:'order_no'},
          {data:'total_item_cd_po'},
          {data:'total_qty_po'},
          {data:'order_date'},
          {data:'po_name'},

          {data:'grn_no'},
          {data:'total_item_cd_grn'},
          {data:'total_grn_qty'},
          {data:'delivery_date'},
          {data:'grn_name'},

          {data:'trx_date'},
          {data:'doc_no'},
          {data:'total_inv_value'},
          {data:'audit_ledger_name'},

          {data:'dr_doc'},
          {data:'dr_date'},
          {data:'total_payment_value'},
          {data:'audit_payment_name'},
        ]
      });
    }

    $('.select2').select2({
      allowClear: true,
      placeholder: '-- Choose --',
    });
    
    $('#entity_project').select2({
      allowClear: true,
      placeholder: '-- Choose --',
      ajax: {
        url: '/five-way/getEntity',
        dataType: 'json',
        delay: 250,
        data: function(params){
          return {
            search: $.trim(params.term),
          }
        },
        processResults: function(response){
          return {
            results: response,
          }
        },
      },
      cache: true,
    }).change(function(){
      if( $(this).val() != '' && $('#type :selected').val() != '' && $('#period').val() != '' ){
        loadTable();
      }
    });

    $('#type').change(function(){
      if( $(this).val() != '' && $('#entity_project :selected').val() != '' && $('#period').val() != '' ){
        loadTable();
      }
    })

    $('#period').daterangepicker({
      locale: {
        format: 'DD/MM/YYYY',
      }
    }).change(function(){
      if( $(this).val() != '' && $('#type :selected').val() != '' && $('#entity_project').val() != '' ){
        loadTable();
      }
    });
  })
</script>