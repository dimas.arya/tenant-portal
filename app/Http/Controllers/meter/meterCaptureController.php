<?php

namespace App\Http\Controllers\meter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon;

class meterCaptureController extends Controller
{
  function index(){
    return view('meter.monitoring.index');
  }

  public function getData()
  {
    // SELECT 
    //         a.entity_project, a.address, a.panel_room, b.debtor_acct, b.debtor_name, a.meter_id, 
    //         ISNULL((SELECT last_read FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -3, getdate()), 'yyyy-MM')), 0) as last_pre_prev_read,
    //         ISNULL((SELECT last_read_high FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -3, getdate()), 'yyyy-MM')), 0) as last_pre_prev_read_high,
    //         ISNULL((SELECT last_read_date FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -3, getdate()), 'yyyy-MM')), DATEADD(month, -3, getdate())) as last_pre_prev_read_date,
            
    //         ISNULL((SELECT last_read FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -2, getdate()), 'yyyy-MM')), 0) as pre_prev_read,
    //         ISNULL((SELECT last_read_high FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -2, getdate()), 'yyyy-MM')), 0) as pre_prev_read_high,
    //         ISNULL((SELECT last_read_date FROM bms_meter where status = 'I' AND meter_id = a.meter_id and FORMAT(curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -2, getdate()), 'yyyy-MM')), DATEADD(month, -2, getdate())) as pre_prev_read_date,

    //         b.last_read as prev_read, b.last_read_high as prev_read_high, b.last_read_date as prev_read_date,
    //         b.curr_read as last_read, b.curr_read_high as last_read_high, b.curr_read_date as last_read_date,
            
    //         (SELECT TOP 1 lwbp FROM meter_capture_elc WHERE entity_project = a.entity_project AND address = a.address AND panel_room = a.panel_room ORDER BY created_at DESC) as curr_read,
    //         (SELECT TOP 1 wbp FROM meter_capture_elc WHERE entity_project = a.entity_project AND address = a.address AND panel_room = a.panel_room ORDER BY created_at DESC) as curr_read_high,
    //         (SELECT TOP 1 created_at FROM meter_capture_elc WHERE entity_project = a.entity_project AND address = a.address AND panel_room = a.panel_room ORDER BY created_at DESC) as curr_read_date
    //         FROM meter_capture_elc_mapping a
    //         JOIN bms_meter b ON a.entity_project = CONCAT(b.entity_cd, b.db_identity) AND a.debtor_acct = b.debtor_acct AND a.meter_id = b.meter_id
    //         WHERE FORMAT(b.curr_read_date, 'yyyy-MM') = FORMAT(DATEADD(month, -1, getdate()), 'yyyy-MM') AND a.entity_project = '".$_GET['entity_project']."'
    //         ORDER BY b.debtor_acct ASC, a.meter_id ASC

    
    $data = DB::select("EXEC sp_auto_meter_capture '".Auth::user()->entity_project."', '".Auth::user()->tenant_code."'");

    $html = '';
    foreach ($data as $key => $data) {
      // 1
      if( ($data->curr_read - str_replace(',', '.', $data->last_read)) < ($data->last_read - str_replace(',', '.', $data->prev_read)) ){
        $color_read = '#2ecc71';
        $arrow_read = '<span style="color: '.$color_read.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read = '#e74c3c';
        $arrow_read = '<span style="color: '.$color_read.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      if( ($data->curr_read_high - str_replace(',', '.', $data->last_read_high)) < ($data->last_read_high - str_replace(',', '.', $data->prev_read_high)) ){
        $color_read_high = '#2ecc71';
        $arrow_read_high = '<span style="color: '.$color_read_high.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read_high = '#e74c3c';
        $arrow_read_high = '<span style="color: '.$color_read_high.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      // 2
      if( ($data->last_read - str_replace(',', '.', $data->prev_read)) < ($data->prev_read - str_replace(',', '.', $data->pre_prev_read)) ){
        $color_read_prev = '#2ecc71';
        $arrow_read_prev = '<span style="color: '.$color_read_prev.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read_prev = '#e74c3c';
        $arrow_read_prev = '<span style="color: '.$color_read_prev.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      if( ($data->last_read_high - str_replace(',', '.', $data->prev_read_high)) < ($data->prev_read_high - str_replace(',', '.', $data->pre_prev_read_high)) ){
        $color_read_high_prev = '#2ecc71';
        $arrow_read_high_prev = '<span style="color: '.$color_read_high_prev.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read_high_prev = '#e74c3c';
        $arrow_read_high_prev = '<span style="color: '.$color_read_high_prev.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      // 3
      if( ($data->prev_read - str_replace(',', '.', $data->pre_prev_read)) < ($data->pre_prev_read - str_replace(',', '.', $data->last_pre_prev_read)) ){
        $color_read_prev_last = '#2ecc71';
        $arrow_read_prev_last = '<span style="color: '.$color_read_prev_last.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read_prev_last = '#e74c3c';
        $arrow_read_prev_last = '<span style="color: '.$color_read_prev_last.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      if( ($data->prev_read_high - str_replace(',', '.', $data->pre_prev_read_high)) < ($data->prev_read_high - str_replace(',', '.', $data->last_pre_prev_read_high)) ){
        $color_read_high_prev_last = '#2ecc71';
        $arrow_read_high_prev_last = '<span style="color: '.$color_read_high_prev_last.'"><i class="fa fa-arrow-circle-down"></i></span>';
      }else{
        $color_read_high_prev_last = '#e74c3c';
        $arrow_read_high_prev_last = '<span style="color: '.$color_read_high_prev_last.'"><i class="fa fa-arrow-circle-up"></i></span>';
      }

      $html .= '
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr style="background: #F2EDFF;">
                  <th scope="col" style="border: 0px !important"><strong>Meter ID - '.$data->meter_id.'</strong></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <table class="table table-bordered">
                      <thead>
                        <tr class="active">
                          <th width="20%">Month</th>
                          <th width="15%">LWBP / WBP</th>
                          <th width="20%">St Awal</th>
                          <th width="20%">St Akhir</th>
                          <th width="20%">Usage (kwh)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td rowspan="2" style="vertical-align: middle">'.Carbon::parse($data->curr_read_date)->format('F').' <br> '.Carbon::parse($data->curr_read_date)->format('d/m/Y H:i:s').'</td>
                          <td>LWBP</td>
                          <td style="text-align: right">'.number_format($data->last_read, 3).'</td>
                          <td style="text-align: right">'.number_format($data->curr_read, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read.'">'.number_format( ($data->curr_read - str_replace(',', '.', $data->last_read)), 3 ).'</span>&nbsp;'.$arrow_read.'</td>
                        </tr>
                        <tr>
                          <td>WBP</td>
                          <td style="text-align: right">'.number_format($data->last_read_high, 3).'</td>
                          <td style="text-align: right">'.number_format($data->curr_read_high, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read_high.'">'.number_format( ($data->curr_read_high - str_replace(',', '.', $data->last_read_high)), 3 ).'</span>&nbsp;'.$arrow_read_high.'</td>
                        </tr>
                        <tr>
                          <td rowspan="2" style="vertical-align: middle">'.Carbon::parse($data->last_read_date)->format('F').'<br>'.Carbon::parse($data->last_read_date)->format('d/m/Y').'</td>
                          <td>LWBP</td>
                          <td style="text-align: right">'.number_format($data->prev_read, 3).'</td>
                          <td style="text-align: right">'.number_format($data->last_read, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read_prev.'">'.number_format( ($data->last_read - str_replace(',', '.', $data->prev_read)), 3 ).'</span>&nbsp;'.$arrow_read_prev.'</td>
                        </tr>
                        <tr>
                          <td>WBP</td>
                          <td style="text-align: right">'.number_format($data->prev_read_high, 3).'</td>
                          <td style="text-align: right">'.number_format($data->last_read_high, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read_high_prev.'">'.number_format( ($data->last_read_high - str_replace(',', '.', $data->prev_read_high)), 3 ).'</span>&nbsp;'.$arrow_read_prev.'</td>
                        </tr>
                        <tr>
                          <td rowspan="2" style="vertical-align: middle">'.Carbon::parse($data->prev_read_date)->format('F').'<br>'.Carbon::parse($data->prev_read_date)->format('d/m/Y').'</td>
                          <td>LWBP</td>
                          <td style="text-align: right">'.number_format($data->pre_prev_read, 3).'</td>
                          <td style="text-align: right">'.number_format($data->prev_read, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read_prev_last.'">'.number_format( ($data->prev_read - str_replace(',', '.', $data->pre_prev_read)), 3 ).'</span>&nbsp;'.$arrow_read_prev_last.'</td>
                        </tr>
                        <tr>
                          <td>WBP</td>
                          <td style="text-align: right">'.number_format($data->pre_prev_read_high, 3).'</td>
                          <td style="text-align: right">'.number_format($data->prev_read_high, 3).'</td>
                          <td style="text-align: right"><span style="color: '.$color_read_high_prev_last.'">'.number_format( ($data->prev_read_high - str_replace(',', '.', $data->pre_prev_read_high)), 3 ).'</span>&nbsp;'.$arrow_read_high_prev_last.'</td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      ';
    }

    return $html;
  }
}
