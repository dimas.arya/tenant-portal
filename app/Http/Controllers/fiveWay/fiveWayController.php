<?php

namespace App\Http\Controllers\fiveWay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon;
use DataTables;

class fiveWayController extends Controller
{
  public function index()
  {
    return view('fiveWay.index');
  }

  public function getEntity(Request $request)
  {
    $search = $request->search;

    $query = DB::table('ifca_cf_entity');
    if($search == ''){
      $data = $query->get();
    }else{
      $data = $query->whereRaw("CONCAT(initial_code, ' - ', entity_name) LIKE '%$search%'")->get();
    }

    $response = array();
    foreach($data as $data){
      $response[] = array(
        "id"      =>  $data->entity_project,
        "text"    =>  $data->initial_code.' - '.$data->entity_name
      );
    }

    return response()->json($response);
  }

  public function listData(Request $request)
  {
    $period = explode(' - ', $request->period);
    $user = Auth::user()->email;
    $user = 'muhammad.dhuharis@mmmproperty.com';
    // $user = 'dimas.putra@mmproperty.com';
    $data = DB::select("SET NOCOUNT ON; EXEC dbo.sp_rpt_fiveway  'LIST', '$request->type', '$request->entity_project', '".Carbon::createfromformat('d/m/Y', $period[0])->format('Y-m-d')."', '".Carbon::createfromformat('d/m/Y', $period[1])->format('Y-m-d')."', '$user'");
    // dd("EXEC dbo.sp_rpt_fiveway  'LIST', '$request->type', '$request->entity_project', '".Carbon::createfromformat('d/m/Y', $period[0])->format('Y-m-d')."', '".Carbon::createfromformat('d/m/Y', $period[1])->format('Y-m-d')."', '$user'");
    return DataTables::of($data)->make(true);
  }
}
